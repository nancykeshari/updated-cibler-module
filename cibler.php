<?php
if (!defined('_PS_VERSION_')) {
    exit;
}

class Cibler extends Module
{
    public function __construct()
    {
        $this->name = 'cibler';
        $this->tab = 'analytics_stats';
        $this->version = '1.1.0.7';
        $this->author = 'CibleR SAS';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => '1.7');
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Cibler');
        $this->description = $this->l('Avec la solution CIBLER, maximiser le R.O.I de votre base client grâce à l’intelligence artificielle.');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

        if (!Configuration::get('CIBLER_CUSTOMERID') || !Configuration::get('CIBLER_APIKEY') || !Configuration::get('CIBLER_ENV')) {
            $this->warning = $this->l('No name provided');
        }
    }


    public function install()
    {
        if (!parent::install()
            || !$this->registerHook('footer')
            || !$this->registerHook('actionOrderStatusPostUpdate')
            || !$this->registerHook('actionCartSave')
            || !$this->installDb()
            || !$this->registerHook('actionCustomerAccountAdd')) {
            return false;
        }

        return true;
    }


    /*
    * module: cibler
    * date: 2019-01-07 03:40:20
    * version: 1.0.3
    * description : Create database for save ciblerId cookie value
    */
    public function installDB()
    {
        return Db::getInstance()->execute('
		CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'wio_cibler`  (
			`id_cibler` INT UNSIGNED NOT NULL AUTO_INCREMENT,
			`cookie` VARCHAR(255) NOT NULL,
			`id_cart` INT NOT NULL,
			PRIMARY KEY (`id_cibler`)
			) DEFAULT CHARSET=utf8;');
    }


    /*
    * module: cibler
    * date: 2019-01-07 03:40:20
    * version: 1.0.3
    * description : uninstall module
    */
    public function uninstall()
    {
        if (!parent::uninstall() ||
            !Configuration::deleteByName('CIBLER_CUSTOMERID') ||
            !Configuration::deleteByName('CIBLER_ENV') ||
            !Db::getInstance()->Execute('DROP TABLE `'._DB_PREFIX_.'wio_cibler`')	 ||
            !Configuration::deleteByName('CIBLER_APIKEY')
          ) {
            return false;
        }

        return true;
    }

    /*
    * module: cibler
    * date: 2019-01-07 03:40:20
    * version: 1.0.3
    * description : Module configuration interface
    */
    public function getContent()
    {
        $html = '';
        if (isset($_POST['submitCiblir'])) {
            Configuration::updateValue('CIBLER_CUSTOMERID', ((isset($_POST['customerId']) && $_POST['customerId'] != '') ? $_POST['customerId'] : ''), true);
            Configuration::updateValue('CIBLER_APIKEY', ((isset($_POST['apiKey']) && $_POST['apiKey'] != '') ? $_POST['apiKey'] : ''), true);
            Configuration::updateValue('CIBLER_ENV', ((isset($_POST['environnements']) && $_POST['environnements'] != '') ? $_POST['environnements'] : ''), true);
            $html .= ''.$this->l('Configuration mise à jour').'';
        }
        $html .= '
		<style>
		 form#ceblir{
				margin: auto;
				max-width: 700px;
				background: #FFF;
				padding: 10px;
				border: 1px solid #DDD;
			}
		</style>

		<h2>'.$this->displayName.'</h2>
		<form id="ceblir" action="'.$_SERVER['REQUEST_URI'].'" method="post">
		<fieldset class="row">
			<label for="customerId" class="col-md-3">'.$this->l('Customer Id').' :</label>
			<input id="customerId" class="col-md-6" name="customerId" value="'.Configuration::get('CIBLER_CUSTOMERID').'">
		</fieldset>
		<fieldset class="row">
			<label for="apiKey" class="col-md-3">'.$this->l('API Key').' :</label>
			<input id="apiKey" class="col-md-6" name="apiKey" value="'.Configuration::get('CIBLER_APIKEY').'">
		</fieldset>
		<fieldset class="row">
			<label for="environnements" class="col-md-3">'.$this->l('Environnements').' :</label>
			<input id="environnements" class="col-md-6" placeholder="https://qa.winitout.com" name="environnements" value="'.Configuration::get('CIBLER_ENV').'">
		</fieldset>
		<input class="btn" type="submit" value="Valider" name="submitCiblir">
		</form>';
        return $html;
    }

    /*
    * module: cibler
    * date: 2019-01-07 03:40:20
    * version: 1.0.3
    * description : Display Cibler context as json object encoded in 64 in a div called cibler_context
    */
    public function hookDisplayFooter($params)
    {
        if (!Configuration::get('CIBLER_CUSTOMERID') || !Configuration::get('CIBLER_APIKEY') || !Configuration::get('CIBLER_ENV')) {
            return;
        }

        $today = getdate();
        $json=array();
        $alternate=array();
        $tag='';
        $user=null;
        $cart=null;
        $page_name = Context::getContext()->controller->php_self;

        // Customer object
        if ($this->context->customer->isLogged()) {
            if ($this->context->customer->id) {
                $customer = new Customer((int) $this->context->customer->id);

                if ($customer) {
                    $user=array(
                                    "email"=>$customer->email,
                                    "marketing_permission"=>true,
                                    "first_name"=>$customer->firstname,
                                    "last_name"=>$customer->lastname,
                                    "customer_reference"=>$customer->id,
                                    "birth_date"=>$customer->birthday
                            );
                }
            }
        }

        //basket
        $context = Context::getContext();
        $products= $context->cart->getProducts();
        //PrestaShopLogger::addLog("Log - ". print_r($today, true) ." - Page Name : ". print_r($page_name, true), 1);
        if (sizeof($products)>0) {
            //PrestaShopLogger::addLog("Log - ". print_r($today, true) ." - Number of product in cart : ". print_r(sizeof($products), true), 1);
            global $currency;
            $items = array();

            foreach ($products as $product) {
                $items[] = array("product_id"=>$product['reference'],
                            "quantity"=>$product['quantity'],
                            "name"=>$product['name'],
                            "category"=>$product['id_category_default'],
                            "unit_price"=> (int) (floatval($product['price_wt'])*100),
                            "price_currency_code"=>$currency->iso_code);
            }

            $total=Context::getContext()->cart->getOrderTotal(true, Cart::BOTH);
            $codes= $this->context->cart->getCartRules();
            $code="";
            if (count($codes)>0) {
                $code=',"code" :"'.$codes[0]["code"].'"';
            }

            $cart = array(
            "total"=>$total,
            "code"=>$code,
            "items"=>$items
          );
        }

        // page type
        $cibler_context = '';
        $my=array("identity","my-account","order-slip","address");

        if (in_array($page_name, $my)) {
            $cibler_context = '"page_type":"customer_zone"';
        }

        if ($page_name == 'product') {
            $id_product =(int)Tools::getValue('id_product');
            $langID = $this->context->language->id;
            $product = new Product($id_product, false, $langID);
            $images=$this->getProductImages($id_product);

            $link = new Link;
            foreach ($images as $m) {
                $alternate[]=$link->getImageLink($product->link_rewrite, $m['id_image'], 'small_default');
            }


            if (Validate::isLoadedObject($product)) {
                global $currency;

                // print_r($product); // This shows all $product data, but I need very little from that.
                $category = new Category((int)$product->id_category_default, (int)$this->context->language->id);

                $image = Image::getCover($id_product);

                $link = new Link;//because getImageLInk is not static function
                $imagePath = $link->getImageLink($product->link_rewrite, $image['id_image'], 'home_default');


                $manufacturer = new Manufacturer($product->id_manufacturer, $langID);
                $json = array(
                    "url"=>$this->context->link->getProductLink($product),
                      "product_id"=>$product->reference, // or $id_product
                      "name"=>$product->name,
                      "image_url"=>$imagePath,
                      "price"=>(int) (floatval($product->price)*100),
                      "price_currency_code"=>$currency->iso_code,
                      "availability"=>$product->available_now,
                      "category"=>$category->id_category,
                      "description"=>htmlentities($product->description),
                      "brand"=>htmlentities($manufacturer->name),
                      "alternate_image_url"=>$alternate

                  );
                $cibler_context = '"page_type":"product_page"';
                $cibler_context = $cibler_context.',"product":'.json_encode($json);
            }
        }


        if ($page_name == 'index') {
            $cibler_context = '"page_type":"home"';
        }

        if ($page_name == 'category') {
            $cibler_context = '"page_type":"product_list"';
        }

        if ($page_name == 'order' or $page_name == 'cart') {
            $cibler_context = '"page_type":"cart"';
        }

        if ($page_name == 'order-confirmation') {
            $cibler_context = '"page_type":"confirmation"';
        }

        if ($cibler_context=='') {
            $cibler_context = '"page_type":"showcase"';
        }

        if ($user!=null) {
            $cibler_context = $cibler_context.',"customer":'.json_encode($user);
        }
        if ($cart!=null) {
            $cibler_context = $cibler_context.',"cart":'.json_encode($cart);
        }
        $code = $this->_decodage('{'.$cibler_context.'}');
        $tag=$code;


        global $smarty;
        $smarty->assign(array(
                    'customerId' => Configuration::get('CIBLER_CUSTOMERID'),
                    'tag'=>$tag,
                    'apikey' => Configuration::get('CIBLER_APIKEY')
                ));
        return $this->display(__FILE__, 'footer.tpl');
    }

    /*
    * module: cibler
    * date: 2019-01-07 03:40:20
    * version: 1.0.3
    * description : Get images url of product
    */
    public static function getProductImages($id_product)
    {
        $id_image = Db::getInstance()->ExecuteS('SELECT `id_image` FROM `'._DB_PREFIX_.'image` WHERE `id_product` = '.(int)($id_product));
        return $id_image;
    }

    /*
    * module: cibler
    * date: 2019-01-07 03:40:20
    * version: 1.0.3
    * description : Save ciblerId cookie value in the cart
    */
    public function hookActionCartSave($params)
    {
        PrestaShopLogger::addLog("hookActionCartSave 2", 1);
        $context = Context::getContext();
        $cartID = (int)$context->cookie->id_cart;
        //PrestaShopLogger::addLog("cartId " . print_r($cartID), 1);
        // echo $cartID;
        if ($cartID != 0) {
            // save cookie id and cart id in db
            $query = 'SELECT COUNT(*) FROM `'._DB_PREFIX_.'wio_cibler` WHERE  id_cart = '.$cartID ;

            $nb = Db::getInstance()->getValue($query);

            if ($nb==0) {
                //PrestaShopLogger::addLog("createLine " . print_r($cartID), 1);
                if (isset($_COOKIE["cibler_id"])) {
                    //PrestaShopLogger::addLog("createLine Cookie" . print_r($_COOKIE["cibler_id"]), 1);
                    $insertData = array(
                                   'id_cart'  => $cartID,
                                   'cookie'  =>  $_COOKIE["cibler_id"]
                                );
                    Db::getInstance()->insert("wio_cibler", $insertData);
                }
            }
        }
    }

    /*
    * module: cibler
    * date: 2019-01-07 03:40:20
    * version: 1.0.3
    * description : Hook for sending order to cibler when order payment is done
    */
    public function hookActionOrderStatusPostUpdate($params)
    {
        PrestaShopLogger::addLog("hookActionOrderStatusPostUpdate " . print_r($params['id_order'], true), 1);
        if (!empty($params['newOrderStatus'])) {
            if ($params['newOrderStatus']->id == Configuration::get('PS_OS_WS_PAYMENT') || $params['newOrderStatus']->id == Configuration::get('PS_OS_PAYMENT')) {
                if (!Configuration::get('CIBLER_CUSTOMERID') || !Configuration::get('CIBLER_APIKEY') || !Configuration::get('CIBLER_ENV')) {
                    return;
                }

                $this->sendOrder($params['id_order']);
            }
        }
    }

    /*
    * module: cibler
    * date: 2019-01-07 03:40:20
    * version: 1.0.3
    * description : Sends order to cibler
    */
    private function sendOrder($oid)
    {
        PrestaShopLogger::addLog("sendOrder " . print_r($oid, true), 1);
        if (empty($oid)) {
            return;
        }

        $order = new Order((int) $oid);
        $address = new Address((int) $order->id_address_delivery);
        $customer = new Customer((int) $order->id_customer);

        $cartId= (int) $order->id_cart;
        $query = 'SELECT cookie FROM `'._DB_PREFIX_.'wio_cibler` WHERE  id_cart = '.$cartId  ;
        $result = Db::getInstance()->getValue($query);
        $cookie = $result ? $result : "not found" ;

        $coupon="";
        $cartRules=$order->getCartRules();
        if (count($cartRules)>0) {
            $coupon= Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('SELECT `code` FROM `'._DB_PREFIX_.'cart_rule` WHERE `id_cart_rule` = \''.$cartRules[0]["id_cart_rule"].'\'');
        }

        $cart = new Cart($order->id_cart);
        $products = $cart->getProducts();

        $allProducts = array();
        foreach ($products as $list) {
            if (!empty(Module::isEnabled('marketplace'))) {
                $sql = 'select id_seller from `' . _DB_PREFIX_ . 'wk_mp_seller_product` where id_ps_product="'.(int)$list['id_product'].'"';
                $result_marketplace_product = Db::getInstance()->getRow($sql);
                if (!empty($result_marketplace_product)) {
                    $seller_id = $result_marketplace_product['id_seller'];
                    $order_type = 'Marketplace';
                } else {
                    $seller_id = 0;
                    $order_type = 'Standard';
                }
            } else {
                $seller_id = 0;
                $order_type = 'Standard';
            }
            $product_array=array(
                'productId' => (int)$list['id_product'],
                'name' => pSQL($list['name']),
                'quantity' => (int)$list['cart_quantity'],
                'category' => (int)$list['id_category_default'],
                'orderType' => pSQL($order_type),
                'unitPrice' => (float)$list['total_wt'],
                'ShelvingId' => 0,
                'SellerId' => (int)$seller_id
            );
            $allProducts[] = $product_array;
        }

        $orderPost=array(
              "orderContext"=>array(
                "key"=> Configuration::get('CIBLER_APIKEY'),
                "email"=>$customer->email,
                "firstName"=>$address->firstname ,
                "lastName"=>$address->lastname ,
                "gender"=>$customer->id_gender,
                "birthday"=>$customer->birthday,
                "country"=>$address->country,
                "postalCode"=>$address->postcode,
                "customerGuid"=>$address->id_customer,
                "cookieId"=>$cookie
              ),
              "orderInformation"=>array(
                "clientStatus"=>"Standard",// si abonné PREMIUM "VIP" sinon "Standard"
                "orderId"=>$order->id,
                "totalSpent"=>$order->total_paid,
                "giftCode"=>$coupon,
                "shippingCost"=>$order->total_shipping,
                "discountAmount"=>$order->total_discounts,
                "orderLines"=>$allProducts,
              )
            );

        $url = Configuration::get('CIBLER_ENV');
        $path = "/api/campaignBehaviors/order/".Configuration::get('CIBLER_CUSTOMERID');


        $data_string = json_encode($orderPost);
        PrestaShopLogger::addLog($data_string, 1);

        $ch = curl_init($url.$path);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(
            $ch,
            CURLOPT_HTTPHEADER,
            array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
        );


        $result = curl_exec($ch);
        PrestaShopLogger::addLog(print_r($result, true), 1);
    }


    /*
    * module: cibler
    * date: 2019-01-07 03:40:20
    * version: 1.0.3
    * description : Sends information to cibler when account is created
    */
    public function hookActionCustomerAccountAdd($patams)
    {
        if (!Configuration::get('CIBLER_CUSTOMERID') || !Configuration::get('CIBLER_APIKEY') || !Configuration::get('CIBLER_ENV')) {
            return;
        }

        if (!isset($patams["newCustomer"])) {
            return;
        }

        $customer=$patams["newCustomer"];
        $Post=array(
                "firstName"=>$customer->firstname,
                "lastName"=>$customer->lastname,
                "email"=>$customer->email,
                "sponsor"=>$_COOKIE["cibler_sponsor"]
                );

        PrestaShopLogger::addLog(print_r($Post, true), 3);

        $url = Configuration::get('CIBLER_ENV');
        $path = "/api/users/customersidesubscription/".Configuration::get('CIBLER_CUSTOMERID');

        $data_string = json_encode($Post);

        $ch = curl_init($url.$path);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(
            $ch,
            CURLOPT_HTTPHEADER,
            array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
        );

        $result = curl_exec($ch);
    }

    /*
    * module: cibler
    * date: 2019-01-07 03:40:20
    * version: 1.0.3
    * description : base64_encode
    */
    private function _decodage($txt)
    {
        return base64_encode($txt);
        // return $txt;
    }
}
