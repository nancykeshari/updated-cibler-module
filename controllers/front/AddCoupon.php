<?php

class ciblerAddCouponModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        parent::initContent();

        $json = file_get_contents('php://input');
				PrestaShopLogger::addLog("AddCoupon " . print_r($json, true), 1);

        $result= json_decode($json);
        if (Configuration::get('CIBLER_APIKEY') != $result->apikey) {
            $error['return'] = 'Error key Api !';
            echo json_encode($error);
            exit();
        }

        if ($this->AddCoupon($json)) {
            $error['return'] = 'Coupon created';
        } else {
            $error['return'] = 'Coupon not created, Please check you Json';
        }

        echo json_encode($error);
        exit();
    }

    // public function postProcess(){}

    public function AddCoupon($json)
    {
        $result= json_decode($json);
        $cart_rule = new CartRule();
        $cart_rule->name = $result->description;  //name (should be an array of every language names with the id_lang as keys);
        $cart_rule->name =array(1=>$result->description) ;

        //Find client id par email
        // $result->user->email ;
        $customer = new Customer();
        if(isset($result->user))
        {
          $cust = $customer->getByEmail($result->user->email);
        }

        if (isset($cust) && isset($cust->id)) {
            $cart_rule->id_customer = $cust->id;
        } else {
            $cart_rule->id_customer = 0;
        }

        $cart_rule->date_from = date('Y-m-d H:i:s');
        $cart_rule->date_to =  date("Y-m-d H:i:s", strtotime($result->expirationDate)) ;
        $cart_rule->description = $result->name;
        $cart_rule->quantity = 1;
        $cart_rule->quantity_per_user = 1;
        $cart_rule->priority = 1;
        $cart_rule->partial_use = 1;
        $cart_rule->code = $result->code ;
        $cart_rule->minimum_amount = $result->threshold;
        $cart_rule->minimum_amount_tax = 1;
        $cart_rule->minimum_amount_currency = 1;
        $cart_rule->minimum_amount_shipping = 0;
        $cart_rule->country_restriction = 0;
        $cart_rule->carrier_restriction = 0;
        $cart_rule->group_restriction = 0;
        $cart_rule->cart_rule_restriction = $result->combinable ? 0 : 1;
        $cart_rule->product_restriction = 0;
        $cart_rule->shop_restriction = 0;
        $cart_rule->free_shipping = 0;
        if ($result->discountType == "PERCENT") {
            $cart_rule->reduction_percent = $result->value;
        }
        if ($result->discountType == "VALUE") {
            $cart_rule->reduction_amount = $result->value;
        }
        $cart_rule->reduction_tax = 1;
        $cart_rule->reduction_currency = 1;
        $cart_rule->reduction_product = 0;
        $cart_rule->gift_product = 0;
        $cart_rule->gift_product_attribute = 0;
        $cart_rule->highlight = 0;
        $cart_rule->active = 1;
        $cart_rule->date_add = date('Y-m-d H:i:s');
        $cart_rule->date_upd = date('Y-m-d H:i:s');
        return $cart_rule->add();
    }
}
